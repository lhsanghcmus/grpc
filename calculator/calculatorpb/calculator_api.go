package calculatorpb

import (
	context "context"
	"fmt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"math"
	"time"
)

type MyServer struct {
}

func (MyServer) Sum(ctx context.Context, req *SumRequest) (*SumResponse, error) {
	fmt.Printf("Sum service was involked with red: %v\n", req)
	a := req.GetFirstNumber()
	b := req.GetSecondNumber()
	result := a + b
	return &SumResponse{
		SumResult: result,
	}, nil
}
func (MyServer) mustEmbedUnimplementedCalculatorServiceServer() {}

func (MyServer) PrimeNumberDecomposition(req *PrimeNumberDecompositionRequest, stream CalculatorService_PrimeNumberDecompositionServer) error {
	n:=req.GetInput()
	var k int32
	k = 2
	for n > 1 {
		if n % k == 0 {
			stream.Send(&PrimeNumberDecompositionResponse{Result:k})
			time.Sleep(1*time.Second)
			n = n / k
		} else {
			k = k + 1
		}
	}
	return nil
}

func (MyServer) SquareRoot(ctx context.Context, req *SquareRootRequest) (*SquareRootResponse,error) {
	fmt.Println("SquareRoot function was invoked")
	number:=req.GetNumber()
	if number < 0 {
		return nil, status.Errorf(codes.InvalidArgument,"Received a negative number: %v",number)
	}
	return &SquareRootResponse{NumberRoot:math.Sqrt(float64(number))},nil
}