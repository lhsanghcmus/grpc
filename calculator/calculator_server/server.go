package main

import (
	"fmt"
	"google.golang.org/grpc/reflection"
	"log"
	"net"

	"gitlab.com/lhsanghcmus/grpc/calculator/calculatorpb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Falied to listen: %v", err)
	}

	s := grpc.NewServer()

	server := calculatorpb.MyServer{}

	calculatorpb.RegisterCalculatorServiceServer(s, server)

	reflection.Register(s)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
