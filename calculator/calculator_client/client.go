package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc/status"
	"io"
	"log"

	"gitlab.com/lhsanghcmus/grpc/calculator/calculatorpb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello I'm a client")
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := calculatorpb.NewCalculatorServiceClient(conn)

//	doUnary(c)
//	doServerStream(c)
	doErrorUnary(c)
}

func doUnary(c calculatorpb.CalculatorServiceClient) {

	fmt.Println("Starting to do a Unary RPC...")

	req := &calculatorpb.SumRequest{
		FirstNumber:  12,
		SecondNumber: 11,
	}

	res, err := c.Sum(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling calculator RPC: %v", err)
	}
	log.Printf("Response from Calculator: %v", res.SumResult)
}

func doServerStream(c calculatorpb.CalculatorServiceClient) {
	fmt.Println("Start to do server stream RPC...")

	req:=&calculatorpb.PrimeNumberDecompositionRequest{Input:120}

	res,err:=c.PrimeNumberDecomposition(context.Background(),req)
	if err != nil {
		log.Fatalf("Error %v",err)
	}
	for {
		msg,err:=res.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error while reading stream %v", err)
		}
		fmt.Printf("Message: %v\n",msg.Result)
	}
}

func doErrorUnary(c calculatorpb.CalculatorServiceClient) {
	doGetSquareRoot(c,-10)
}

func doGetSquareRoot(c calculatorpb.CalculatorServiceClient, n int32) {
	resp,err:=c.SquareRoot(context.Background(),&calculatorpb.SquareRootRequest{Number:n})
	if err != nil {
		respErr,ok:=status.FromError(err)
		if ok {
			fmt.Printf("Code: %v\n",respErr.Code())
			fmt.Printf("Message: %v\n",respErr.Message())
			return
		} else {
			log.Fatalf("Other error: %v\n",respErr)
		}
	}
	fmt.Printf("Result: %v\n",resp.GetNumberRoot())
}