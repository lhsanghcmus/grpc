package main

import (
	"fmt"
	"google.golang.org/grpc/credentials"
	"log"
	"net"

	"gitlab.com/lhsanghcmus/grpc/greet/greetpb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello")

	lis, err := net.Listen("tcp", "0.0.0.0:50051")
	if err != nil {
		log.Fatalf("Falied to listen: %v", err)
	}

	certFile:="ssl/server.crt"
	keyFile:="ssl/server.pem"

	creds,sslErr:=credentials.NewServerTLSFromFile(certFile,keyFile)
	if sslErr != nil {
		log.Fatalf("Erroo while reading file: %v",sslErr)
	}

	opts:=grpc.Creds(creds)

	s := grpc.NewServer(opts)

	server := greetpb.MyServer{}

	greetpb.RegisterGreetServiceServer(s, server)

	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

}
