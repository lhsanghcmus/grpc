package greetpb

import (
	context "context"
	"fmt"
	"io"
	"log"
	"strconv"
	"time"
)

type MyServer struct {
}

func (MyServer) Greet(ctx context.Context, req *GreetRequest) (*GreetResponse, error) {
	fmt.Printf("Greet function was invoked with %v", req)
	firstName := req.GetGreeting().GetFirstName()
	result := "Hello " + firstName
	res := &GreetResponse{
		Result: result,
	}
	return res, nil
}

func (MyServer) GreetManyTimes(req *GreetManyTimesRequest, stream GreetService_GreetManyTimesServer) error {
	fmt.Printf("Greet many times function was invoked with %v", req)
	firstName := req.GetGreeting().GetFirstName()
	for i := 0; i < 10; i++ {
		result := "Hello " + firstName + " number: " + strconv.Itoa(i)
		resp := GreetManyTimesResponse{
			Result: result,
		}
		stream.Send(&resp)
		time.Sleep(1 * time.Second)
	}
	return nil
}

func (MyServer) LongGreet(stream GreetService_LongGreetServer) error {
	fmt.Println("LongGreet function was invoked with a streaming request")
	result:=""
	for {
		req,err:=stream.Recv()
		if err == io.EOF {
			return stream.SendAndClose(&LongGreetResponse{Result:result})
		}
		if err != nil {
			log.Fatalf("Error when reading client stream: %v\n",err)
		}

		firstName:=req.GetGreeting().FirstName
		result+="Hello " + firstName +"! "
	}
}

func (MyServer) GreetEveryOne(stream GreetService_GreetEveryOneServer) error {
	fmt.Println("Greet Everyone function was invoked with a streaming request")

	for {
		req,err:= stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			log.Fatalf("Error while receving data: %v",err)
			return err
		}
		sendErr:=stream.Send(&GreetEveryOneResponse{Result:req.GetGreeting().FirstName + "received!!!!"})
		if sendErr != nil {
			log.Fatalf("Error while sending data: %v",err)
			return err
		}
	}

}

func (MyServer) mustEmbedUnimplementedGreetServiceServer() {}
