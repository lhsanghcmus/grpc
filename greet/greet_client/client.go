package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	"gitlab.com/lhsanghcmus/grpc/greet/greetpb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello I'm a client")
	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("could not connect: %v", err)
	}
	defer conn.Close()

	c := greetpb.NewGreetServiceClient(conn)

//	doUnary(c)
	//doServerStreaming(c)
	//doClientStreaming(c)
	doBiDirectionalStreaming(c)
}

func doUnary(c greetpb.GreetServiceClient) {

	fmt.Println("Starting to do a Unary RPC...")

	req := &greetpb.GreetRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Le Hoang",
			LastName:  "Sang",
		},
	}

	res, err := c.Greet(context.Background(), req)
	if err != nil {
		log.Fatalf("Error while calling greet RPC: %v", err)
	}
	log.Printf("Response from Greet: %v", res.Result)
}

func doServerStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Started to do server streaming RPC...")

	req := &greetpb.GreetManyTimesRequest{
		Greeting: &greetpb.Greeting{
			FirstName: "Le hoang",
			LastName:  "Sang",
		},
	}

	respStream, err := c.GreetManyTimes(context.Background(), req)
	if err != nil {
		log.Fatalf("error: %v", err)
	}
	for {
		msg, err := respStream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("Error while reading stream %v", err)
		}
		log.Printf("%v", msg.String())
	}

}

func doClientStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Started to do server streaming RPC...")
	request:=[]*greetpb.LongGreetRequest{
		&greetpb.LongGreetRequest{Greeting:&greetpb.Greeting{FirstName:"Le Hoang", LastName:"Sang"}},
		&greetpb.LongGreetRequest{Greeting:&greetpb.Greeting{FirstName:"Nguyen Hoang", LastName:"Sang"}},
	}
	stream,err:=c.LongGreet(context.Background())
	if err != nil {
		log.Fatalf("Error while calling LongGreet. %v",err)
	}

	for _,req:=range request {
		fmt.Printf("Sending request: %v\n",req)
		stream.Send(req)
		time.Sleep(1*time.Second)
	}

	res,err:=stream.CloseAndRecv()
	if err != nil {
		log.Fatalf("Error while getting response from LongGreet. %v\n",err)
	}

	fmt.Printf("LongGreet response: %v\n",res)
}

func doBiDirectionalStreaming(c greetpb.GreetServiceClient) {
	fmt.Println("Started to do server streaming RPC...")
	request:=[]*greetpb.GreetEveryOneRequest{
		&greetpb.GreetEveryOneRequest{Greeting:&greetpb.Greeting{FirstName:"Le Hoang", LastName:"Sang"}},
		&greetpb.GreetEveryOneRequest{Greeting:&greetpb.Greeting{FirstName:"Nguyen Hoang", LastName:"Sang"}},
	}
	stream,err:=c.GreetEveryOne(context.Background())
	if err != nil {
		log.Fatalf("Error while creating stream: %v",err)
		return
	}
	waitc:=make(chan struct{})
	go func() {
		for _,req:=range request {
			fmt.Printf("Sending message: %v\n",req)
			stream.Send(req)
			time.Sleep(1000*time.Millisecond)
		}
		stream.CloseSend()
	}()

	go func() {
		for {
			res, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("Error while receiving response: %v\n", err)
				break
			}
			fmt.Printf("Received: %v\n", res)
		}
		close(waitc)
	}()

	<-waitc
}