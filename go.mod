module gitlab.com/lhsanghcmus/grpc

go 1.12

require (
	cloud.google.com/go v0.74.0 // indirect
	cloud.google.com/go/storage v1.12.0 // indirect
	github.com/census-instrumentation/opencensus-proto v0.3.0 // indirect
	github.com/cncf/udpa/go v0.0.0-20201211205326-cc1b757b3edd // indirect
	github.com/creack/pty v1.1.11 // indirect
	github.com/envoyproxy/go-control-plane v0.9.8 // indirect
	github.com/envoyproxy/protoc-gen-validate v0.4.1 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/google/pprof v0.0.0-20201218002935-b9804c9f04c2 // indirect
	github.com/iancoleman/strcase v0.1.2 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/ktr0731/evans v0.9.1 // indirect
	github.com/lyft/protoc-gen-star v0.5.2 // indirect
	github.com/pkg/sftp v1.12.0 // indirect
	github.com/prometheus/client_model v0.2.0 // indirect
	github.com/spf13/afero v1.5.1 // indirect
	github.com/stretchr/objx v0.3.0 // indirect
	github.com/yuin/goldmark v1.3.0 // indirect
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad // indirect
	golang.org/x/exp v0.0.0-20200224162631-6cc2880d07d6 // indirect
	golang.org/x/net v0.0.0-20201216054612-986b41b23924 // indirect
	golang.org/x/sync v0.0.0-20201207232520-09787c993a3a // indirect
	golang.org/x/sys v0.0.0-20201221093633-bc327ba9c2f0 // indirect
	golang.org/x/term v0.0.0-20201210144234-2321bbc49cbf // indirect
	golang.org/x/tools v0.0.0-20201221201019-196535612888 // indirect
	google.golang.org/genproto v0.0.0-20201214200347-8c77b98c765d // indirect
	google.golang.org/grpc v1.34.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.0.1 // indirect
	google.golang.org/protobuf v1.25.0
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)
